package com.designpaterns;

public class CheesePizza extends Pizza {

    @Override
    public String getName() {
        return "Cheese Pizza";
    }
}
