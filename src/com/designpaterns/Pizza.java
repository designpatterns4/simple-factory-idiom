package com.designpaterns;

public abstract class Pizza {
    void box(){

    }

    void cut(){

    }

    void bake(){

    }

    void prepare(){

    }

    public abstract String getName();
}
