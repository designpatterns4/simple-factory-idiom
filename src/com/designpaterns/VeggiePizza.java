package com.designpaterns;

public class VeggiePizza extends Pizza {

    @Override
    public String getName() {
        return "Veggie Pizza";
    }
}
