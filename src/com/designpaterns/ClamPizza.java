package com.designpaterns;

public class ClamPizza extends Pizza {

    @Override
    public String getName() {
        return "Clam Pizza";
    }
}
