package com.designpaterns;

public class PepperoniPizza extends Pizza {
    @Override
    public String getName() {
        return "Pepperoni Pizza";
    }
}
